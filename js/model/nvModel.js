function nhanVien(
    _tkNV,
    _tenNV,
    _email,
    _matKhau,
    _ngayLam,
    _luongCB,
    _chucVu,
    _gioLam) {

    this.tk = _tkNV;
    this.ten = _tenNV;
    this.email = _email;
    this.pass = _matKhau;
    this.ngayLam = _ngayLam;
    this.luongCB = _luongCB * 1;
    this.chucVu = function () {
        var cv = _chucVu;
        if (cv == 1) {
            return "Sếp";
        } else if (cv == 2) {
            return "Trưởng phòng";
        } else if (cv == 3) {
            return "Nhân viên";
        }
       
    };
    this.gioLam = _gioLam;
    // 5. xây dựng phương thức tính tổng lương
    this.tinhTongLuong = function () {
        var temp;
        if (_chucVu == 1) {
            temp = this.luongCB * 3;
        } else if (_chucVu == 2) {
            temp = this.luongCB * 2;
        } else {
            temp = this.luongCB;
        }
        return temp;
    }
    // 6. Xây dựng phương thức xếp loại
    this.xepLoai = function () {
        var xl;
        if (_gioLam >= 192) {
            xl = "Xuất sắc";
        } else if (_gioLam < 192 && _gioLam >= 176) {
            xl = "Giỏi";
        } else if (_gioLam < 176 && _gioLam >= 160) {
            xl = "Khá";
        } else {
            xl = "Trung bình";
        }
        return xl;
    }

}