
function layTT() {
    // lấy thông tin từ phom
    const _tkNV = document.getElementById("tknv").value;
    const _tenNV = document.getElementById("name").value;
    const _email = document.getElementById("email").value;
    const _matKhau = document.getElementById("password").value;
    const _ngayLam = document.getElementById("datepicker").value;
    const _luongCB = document.getElementById("luongCB").value;
    const _chucVu = document.getElementById("chucvu").value;
    const _gioLam = document.getElementById("gioLam").value;

    // tạo đối tượng nhân viên
    var nv=new nhanVien(
        _tkNV,
        _tenNV,
        _email,
        _matKhau,
        _ngayLam,
        _luongCB,
        _chucVu,
        _gioLam
    );
return nv;

}


//1. Render dữ liệu ra bảng
function renderTable(nhanVienArr) {
    var content="";
        for (let index = 0; index < nhanVienArr.length; index++) {
       var item = nhanVienArr[index];
              var contentTr=`<tr>
       <td>${item.tk}</td>
       <td>${item.ten}</td>
       <td>${item.email}</td>
       <td>${item.ngayLam}</td>
       <td>${item.chucVu()}</td>
       <td>${item.tinhTongLuong()}</td>
       <td>${item.xepLoai()}</td>
       <td>
           <button class="btn btn-danger" onclick="xoaNV('${item.tk}')"><i class="fa fa-trash-alt"></i></button>
           <button class="btn btn-success" onclick="xemNV('${item.tk}')"><i class="fa fa-user-edit"></i></button>
       </td>
   </tr>`;
   content += contentTr;
    }
    getEl("tableDanhSach").innerHTML=content;
}


// Show thông tin lên form modal
function showFormNV(nv) {
    // Mở modal
    $('#myModal').modal('show');
    // gán thông tin
    getEl("tknv").value=nv.tk;
    getEl("name").value=nv.ten;
    getEl("email").value=nv.tk;
    getEl("pass").value=nv.pass;
    getEl("datepicker").value=nv.ngayLam;
    getEl("luongCB").value=nv.luongCB;
    getEl("chucvu").value=nv.chucVu;
    getEl("gioLam").value=nv.gioLam;

    
}

function getLocale(id, arr) {
    var viTri = -1;

  for (var index = 0; index < arr.length; index++) {
    var sv = arr[index];
    if (sv.ma == id) {
      viTri = index;
      break;
    }
  }
  return viTri;
}