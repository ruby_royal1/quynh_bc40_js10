function getEl(id) {
    return document.getElementById(id);

}
// 4. Validate 

// Check trùng
function checkDump(idNV, nvArr) {
    var index = nvArr.findIndex(function (item) {
        return idNV == item.tk;
    })
    if (index == -1) {
        getEl("tknv").innerText = "";
        return true;
    } else {
        getEl("tknv").innerText = "Mã nhân viên bị trùng, vui lòng kiểm tra lại";
        getEl("tknv").style.display="block";
    }
}

// kiểm tra độ dài

function checkLeng(value, idErr, max, min) {
    var length = value.length;
    if (length < min || length > max) {
        document.getElementById(idErr).innerText = `Độ dài phải tử ${min} đến ${max} ký tự`;
        getEl(idErr).style.display="block";
        return false;
    } else {
        document.getElementById(idErr).innerText == '';
        return true

    }
}

// kiểm tra rỗng
function checkNull(val, idErr) {
    // var isnull = true;
    var vl = getEl(val).value;
    if (vl == "") {
        getEl(idErr).innerText = "Không được để trống trường này"
        getEl(idErr).style.display="block";
        return false;
    } else {
        getEl(idErr).innerText = "";
        return true
    }

}

// Kiểm tra chuỗi phải là chữ
function letterCheck(value) {
    const reg = /^[a-zA-Z]/;
    var isLet = reg.test(value);
    if (isLet) {
        getEl("tbTen").innerText = '';
        return true;
    } else {
        getEl("tbTen").innerText = 'Tên không hợp lệ!';
        getEl("tbTen").style.display="block";
        return false;
    }
}

// Kiểm tra tên phải là số
function numCheck(value) {
    const reg = /^\d+$/;
    var isNum = reg.test(value);
    if (isNum) {
        getEl("tbTKNV").innerText = '';
        return true;
    } else {
        getEl("tbTKNV").innerText = 'Mã nhân viên không hợp lệ!';
        getEl("tbTKNV").style.display="block";
        return false;
    }
}

// Check password 6-10 ký tự...
function checkPass(value) {
    const reg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,10}$/;
    var isPass = reg.test(value);
    if (isPass) {
        getEl("tbMatKhau").innerText = '';
        return true;
    } else {
        getEl("tbMatKhau").innerText = 'Password không hợp lệ!';
        getEl("tbMatKhau").style.display="block";
        return false;

    }
}
// Check email
function checkEmail(value) {
    const reg =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    var isEmail=reg.test(value);
    if(isEmail){
      document.getElementById("tbEmail").innerText="";
      return true;
    } else{
      document.getElementById("tbEmail").innerText='Email không hợp lệ';
      getEl("tbEmail").style.display="block";
      return false;
    }
  }
//   Check lương
function checkBetween(idVl,idErr,min,max) {
    var a=getEl(idVl).value*1;
    if (a<min||a>max) {
        getEl(idErr).innerText="Vui lòng nhập số hợp lệ";
        getEl(idErr).style.display="block";
        return false;
    } else{
        getEl(idErr).innerText="";
        return true;
    }
}
// Check chức vụ
function checkCV() {
a=getEl("chucvu").value;
    if (a==1||a==2||a==3) {
        getEl("tbChucVu").innerText="";
        return true;

        
    } else{
        getEl("tbChucVu").innerText="Vui lòng chọn chức vụ";
        getEl("tbChucVu").style.display="block";
        return false;
    }
}